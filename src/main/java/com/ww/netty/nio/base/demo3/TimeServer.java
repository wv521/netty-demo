package com.ww.netty.nio.base.demo3;

public class TimeServer {
    public static void main(String[] args) {
        int port=8081; //服务端默认端口
        TimeServerHandler timeServer=new TimeServerHandler(port);
        new Thread(timeServer, "NIO-MultiplexerTimeServer-001").start();
    }
}
